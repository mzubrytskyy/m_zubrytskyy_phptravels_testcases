package net.phptravels_signup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.tools.Browser;

public class SignUpFieldsValidation {

	private WebDriver driver;

	@Test
	public void test_SignUpEmptyFieldValidation() {

		driver = Browser.open();

		String baseURL = "http://www.phptravels.net/";
		driver.get(baseURL);

		waitFor(2);
		WebElement myAccountButton = driver.findElement(By.cssSelector(".navbar-static-top #li_myaccount > a"));
		myAccountButton.click();

		WebElement dropdown = driver.findElement(By.cssSelector(".navbar-static-top #li_myaccount > a + .dropdown-menu"));

		WebElement loginOption = dropdown.findElement(By.xpath(".//a[contains(text(),'Sign Up')]"));
		loginOption.click();
		
		WebElement signupButton = driver.findElement(By.cssSelector(".signupbtn"));
		signupButton.click();
		
		waitFor(2);
		WebElement actualEmailEmptyField = driver.findElement(By.xpath("//div[@class='alert alert-danger']/p[1]"));
		String expectedEmailEmptyField = "The Email field is required.";
		Assert.assertEquals(actualEmailEmptyField.getText(), expectedEmailEmptyField, "Incorrect message for Email");

		WebElement actualPasswordEmptyField = driver.findElement(By.xpath("//div[@class='alert alert-danger']/p[2]"));
		String expectedPasswordEmptyField = "The Password field is required.";
		Assert.assertEquals(actualPasswordEmptyField.getText(), expectedPasswordEmptyField, "Incorrect message for Password");
		
		WebElement actualConfirmPasswordEmptyField = driver.findElement(By.xpath("//div[@class='alert alert-danger']/p[3]"));
		String expectedConfirmPasswordEmptyField = "The Password field is required.";
		Assert.assertEquals(actualConfirmPasswordEmptyField.getText(), expectedConfirmPasswordEmptyField, "Incorrect message for Confirm Password");
		
		WebElement actualFirstNameEmptyField = driver.findElement(By.xpath("//div[@class='alert alert-danger']/p[4]"));
		String expectedFirstNameEmptyField = "The First name field is required.";
		Assert.assertEquals(actualFirstNameEmptyField.getText(), expectedFirstNameEmptyField, "Incorrect message for First Name");
		
		WebElement actualLastNameEmptyField = driver.findElement(By.xpath("//div[@class='alert alert-danger']/p[5]"));
		String expectedLastNameEmptyField = "The Last name field is required.";
		Assert.assertEquals(actualLastNameEmptyField.getText(), expectedLastNameEmptyField, "Incorrect message for Last Name");
	}
	
//	@Test
//	public void test_SignUpEmptyFieldValidation() {

//	@AfterMethod(alwaysRun = true)
//	public void tearDown() {
//		if (driver != null) {
//			driver.quit();
//		}
//	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
