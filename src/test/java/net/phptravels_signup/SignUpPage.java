package net.phptravels_signup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignUpPage {

		private WebDriver driver;
		
	public SignUpPage(WebDriver driver) {
		this.driver = driver;
	}

	public void signUpAs(String firstName, String lastName, String phoneNumber, String emailAddress, String password,
			String confirmpassword) {
		FirstName(firstName);
		LastName(lastName);
		PhoneNumber(phoneNumber);
		EmailAddress(emailAddress);
		Password(password);
		ConfirmPassword(confirmpassword);
		ClickSignupButton();
	}

	public void FirstName(String firstName){
		WebElement userFirstNameField = driver.findElement(By.name("firstname"));
		userFirstNameField.sendKeys(firstName);
	}
	public void LastName(String lastName){
		WebElement userLastNameField = driver.findElement(By.name("lastname"));
		userLastNameField.sendKeys(lastName);
	}
	public void PhoneNumber(String phoneNumber){
		WebElement userPhoneField = driver.findElement(By.name("phone"));
		userPhoneField.sendKeys(phoneNumber);
	}
	public void EmailAddress(String emailAddress){
		WebElement userEmailField = driver.findElement(By.name("email"));
		userEmailField.sendKeys(emailAddress);
	}
	public void Password(String password){
		WebElement userPasswordField = driver.findElement(By.name("password"));
		userPasswordField.sendKeys(password);
	}
	public void ConfirmPassword(String confirmpassword){
		WebElement userConfirmPasswordField = driver.findElement(By.name("confirmpassword"));
		userConfirmPasswordField.sendKeys(confirmpassword);
	}
	public void ClickSignupButton(){
		WebElement signupButton = driver.findElement(By.cssSelector(".signupbtn"));
		signupButton.click();
	}
	
	
}
