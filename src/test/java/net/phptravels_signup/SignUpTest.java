package net.phptravels_signup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import com.github.javafaker.Faker;
import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.tools.Browser;

public class SignUpTest {

	private PhpTravelsApp phpTravelsApp = new PhpTravelsApp();
	private WebDriver driver;
	private MainPage mainPage;
	private SignUpPage signUpPage;

	@Test
	public void test_SignUp() {

		mainPage = phpTravelsApp.openMainPage();
		waitFor(2);
		signUpPage = mainPage.openSignUpPage();

		Faker faker = new Faker();
		String firstName = faker.name().firstName();
		String lastName = faker.name().lastName();
		String phoneNumber = faker.phoneNumber().phoneNumber();
		String emailAddress = faker.internet().emailAddress();
		String password = "111111";
		String confirmpassword = "111111";
		
		signUpPage.signUpAs(firstName, lastName, phoneNumber, emailAddress, password, confirmpassword);
		
//		waitFor(4);
//		String expectedUrl = "http://www.phptravels.net/account/";
//		String actualUrl = driver.getCurrentUrl();
//
//		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");
//
//		WebElement usernameOnHeader = driver.findElement(
//				By.cssSelector(".currency_btn>.sidebar>li:nth-of-type(1)>a.dropdown-toggle.go-text-right"));
//		String actualUserNameOnHeader = usernameOnHeader.getText();
//
//		Assert.assertEquals(actualUserNameOnHeader, firstName.toUpperCase(), "Incorrect username");
//
//		String expectedFullName = "Hi, " + firstName + " " + lastName;
//
//		WebElement actualFullNameElement = driver.findElement(By.xpath("//h3[contains(text(),'Hi, ')]"));
//		Assert.assertEquals(actualFullNameElement.getText(), expectedFullName, "Incorrect username");
//
	}

	 @AfterMethod(alwaysRun = true)
	 public void tearDown() {
	 if (driver != null){
		 driver.quit();
	 }
	 }

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
