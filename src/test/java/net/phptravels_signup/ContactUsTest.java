package net.phptravels_signup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.github.javafaker.Faker;
import com.qagroup.tools.Browser;

public class ContactUsTest {

	private WebDriver driver;

	@Test
	public void testContactUs() {

		driver = Browser.open();
		String baseURL = "http://www.phptravels.net/";
		driver.get(baseURL);

		//waitFor(2);
		WebElement contactUs = driver.findElement(By.xpath("html/body/nav/div/div/div/ul/li[8]"));
		contactUs.click();

		Faker faker = new Faker();

		WebElement nameField = driver.findElement(By.name("contact_name"));
		nameField.sendKeys(faker.name().fullName());

		WebElement emailField = driver.findElement(By.name("contact_email"));
		emailField.sendKeys(faker.internet().emailAddress());

		WebElement subjectField = driver.findElement(By.name("contact_subject"));
		subjectField.sendKeys(faker.commerce().productName());

		WebElement messageField = driver.findElement(By.name("contact_message"));
		messageField.sendKeys(faker.lorem().sentence(10));

		 driver.findElement(By.name("submit_contact")).click();
		
		String expectedSuccessfullMessage = "Message Sent Successfully";
		String actualSuccessfullMessage = driver.findElement(By.cssSelector(".alert-success")).getText();
		
		Assert.assertEquals(actualSuccessfullMessage, expectedSuccessfullMessage, "Incorrect message");
	}

	 @AfterMethod(alwaysRun = true)
	 public void tearDown() {
	 if (driver != null){
	 driver.quit();
	 waitFor(2);
	 }
	 }

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
