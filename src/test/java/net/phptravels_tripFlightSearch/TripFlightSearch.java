package net.phptravels_tripFlightSearch;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.qagroup.tools.Browser;

public class TripFlightSearch {
	
	private WebDriver driver;
	
	@Test
	public void test_tripFlightSearch() {

		driver = Browser.open();

		String baseURL = "http://www.phptravels.net/";
		driver.get(baseURL);
		
		WebElement flightsTab = driver.findElement(By.xpath("//span[contains(text(),'Flights')]"));
		flightsTab.click();
		
		WebElement roundTripRadioButton = driver.findElement(By.xpath("//div[@class='iradio_square-grey']"));
		roundTripRadioButton.click();
		
		WebElement originPlace = driver.findElement(By.xpath("//div[@class='select2-container widget-select2']/a[@class='select2-choice select2-default']/span[text()='Enter Location']"));
		originPlace.click();
		WebElement originPlace2 = driver.findElement(By.cssSelector("#select2-drop .select2-search .select2-input"));
		originPlace2.sendKeys("Lviv");
		
		WebElement locateDrodown = driver.findElement(By.xpath("//div[@id='s2id_autogen10']/a/span[@class='select2-arrow']"));
		
		waitFor(2);
		WebElement optionToSelect = driver.findElement(By.xpath("//div[contains(text(), 'International Arpt - (LWO)')]"));
		optionToSelect.click();
		
		
//		WebElement originPlace1 = driver.findElement(By.xpath("//div/input[class contains 'select2-focused']"));
//		originPlace1.sendKeys("Lviv");
//		WebElement destinationPlace = driver.findElement(By.cssSelector("#s2id_autogen14"));
	}
	
//	@AfterMethod(alwaysRun = true)
//	public void tearDown() {
//		if (driver != null) {
//			driver.quit();
//		}
//	}

	private void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
