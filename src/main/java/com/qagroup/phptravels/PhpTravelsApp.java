package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.qagroup.tools.Browser;

import net.phptravels_signup.SignUpPage;

public class PhpTravelsApp {
	
	private WebDriver driver;

	public PhpTravelsApp() {
		
	}
	
	public MainPage openMainPage() {
		driver = Browser.open();
		String baseURL = "http://www.phptravels.net/";
		driver.get(baseURL);
		return new MainPage(driver);
		
	}
}
