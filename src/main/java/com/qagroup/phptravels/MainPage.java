package com.qagroup.phptravels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.phptravels_signup.SignUpPage;

public class MainPage {

	private WebDriver driver;

	public MainPage(WebDriver driver) {
		this.driver = driver;
	}

	public SignUpPage openSignUpPage() {
		
		WebElement myAccountButton = driver.findElement(By.cssSelector(".navbar-static-top #li_myaccount > a"));
		myAccountButton.click();

		WebElement dropdown = driver
				.findElement(By.cssSelector(".navbar-static-top #li_myaccount > a + .dropdown-menu"));

		WebElement loginOption = dropdown.findElement(By.xpath(".//a[contains(text(),'Sign Up')]"));
		loginOption.click();
		
		return new SignUpPage(driver);
	}
		

}
