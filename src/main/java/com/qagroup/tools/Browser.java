package com.qagroup.tools;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;

public class Browser {

	public static WebDriver open(){

		WebDriver driver = startBrowser();
		driver.manage().window().maximize();
		return driver;
	}

	private static WebDriver startBrowser() {
		String browser = System.getProperty("browser");
		WebDriver driver = null;
		if (browser == null || "chrome".equals(browser.toLowerCase())) {
			driver = startChrome();
		} else if ("firefox".equals(browser.toLowerCase())) {
			driver = startFireFox();

		} else
			throw new RuntimeException("\nUnsupported driver for browser: " + browser + "\n");
		return driver;
	}

	private static WebDriver startChrome() {
		ChromeDriverManager.getInstance().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("disable-infobars");
		return new ChromeDriver(options);
	}

	private static WebDriver startFireFox() {
		FirefoxDriverManager.getInstance().setup();
		return new FirefoxDriver();
	}

}
